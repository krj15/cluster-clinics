"""
This script contains all the code used to analyse the distance travel distance to RACH and cluster clinics for children
living in Aberdeen City and Shire. If running this code, please silence the appropriate sections of code.

To run this, centresurv must be saved in the same respository as this file in a file called centresurv. This can be
downloaded from gitlabs Project ID: 13210238
"""


import pandas as pd
import geoserv.postcode_setup as ps
import geoserv.analyse as analyse
import geoserv.maps as maps
import numpy as np
import scipy.stats as stats
from matplotlib import pyplot as plt

#########################################################

LADs = ["Aberdeen City", "Aberdeenshire"]  # Local authority areas to be included
rach = ["AB25 2ZG"]
clinics = ["AB22 8ZP", "AB11 7XH", "AB24 2AS", "AB14 0RP", "AB45 1JA", "AB42 2XB", "AB51 3UL", "AB39 2NJ", "AB33 8FL"]  # List of all clinics
all_centres = clinics + rach
area = "Scotland"

##########################################################

# # Preparation of dataset
# pc_df = pd.read_csv("ONSPD_NOV_2020_UK.csv")  # (Source: https://geoportal.statistics.gov.uk/datasets/national-statistics-postcode-lookup-november-2020/about)
# pc_df.rename(columns={"pcds": "Postcode", "lat": "Latitude", "long": "Longitude", "lsoa11": "Data Zone"}, inplace=True)
#
# pop_df = pd.read_csv("POSTCODE_TO_OA.csv")  # (Source: https://www.nrscotland.gov.uk/statistics-and-data/geography/our-products/census-datasets/2011-census/2011-indexes)
# pop_df.rename(columns={"PopulationCount": "Population"}, inplace=True)
#
# combined_df = pc_df.merge(pop_df, on="Postcode")
#
# all_clinics = rach + clinics
# combined_df = combined_df.append(pc_df.loc[pc_df["Postcode"].isin(all_clinics)])
# combined_df = combined_df.drop_duplicates(subset=["Postcode"])
#
# lad_df = pd.read_csv("LA_UA names and codes UK as at 04_20.csv")
# lad_df.rename(columns={"LAD20NM": "Local Authority Name"}, inplace=True)
# lad_codes = list(lad_df["LAD20CD"])
# lad_nms = list(lad_df["Local Authority Name"])
#
# for code, name in zip(lad_codes, lad_nms):
#     combined_df.loc[combined_df.oslaua == code, "oslaua"] = name
#
# combined_df = combined_df[combined_df.oslaua.isin(LADs)]
# combined_df.rename(columns={"oslaua": "Local Authority Name"}, inplace=True)
#
# combined_df.to_csv("ab_postcodes.csv")  # This is the csv file containing only the appropriate postcodes and data
#
# print(sum(combined_df["Population"]))  # Total population of analysis
# print(len(combined_df["Postcode"]))  # Total number of postcodes in analysis
#
# ###########################################
#
# # Calculate shortest paths to RACH and to Cluster Clinics.
#
# columns = {"Local Authority Name": "VARCHAR", "imd": "INTEGER", "Data Zone": "VARCHAR"}
#
# ps.postcode_to_db(area, "ab_postcodes.csv",
#                   setup_level="custom",
#                   columns=columns)
#
# graph = ps.load_graph(area)
# ps.nearest_node(graph, area)
# ps.shortest_paths2(graph, area, rach)  # calculate shortest paths to RACH
# ps.shortest_paths(graph, area, clinics, "CC_Distance", "Clinic")  # calculate shortest paths to cluster clinics
# ps.shortest_paths(graph, area, all_centres, "All_Distance", "All_Centre")  # calculate shortest path to either cluster clinic or RACH

# ##############################################

# Analyse unweighted output data - i.e. travel distance for all people in Grampian

# print(analyse.median_distance(area))  # print overall median distance to RACH
# print(analyse.median_distance(area, distance="CC_Distance"))  # print overall median distance to cluster clinics
# analyse.median_distance(area, "Data Zone", graph=True, distance="CC_Distance")  # Median distance by data zone
# print(analyse.gini(area, False, comparison="CC_Distance"))  # print gini coefficient for access to cluster clinics
# maps.heatmap(area, metric="population")  # create heatmap showing population distribution in Grampian
# maps.heatmap(area, centres=clinics, metric="population")  # create heatmap showing population distribution and clinics
# print(analyse.single_step_analysis(area, "Data Zone"))
# ss_df = analyse.single_step_analysis(area, "Data Zone", "Distance")
# ss_df.to_csv("RACH_Datazones.csv")  # create csv file containing data aggregated by small data zone
#
#################################################
#
# # Create choropleth maps showing median travel distance for each small data zone - maps will save as html files
# # Shapefiles downloaded to same directory Source: https://spatialdata.gov.scot/geonetwork/srv/eng/catalog.search#/metadata/7d3e8709-98fa-4d71-867c-d5c8293823f2
# maps.choropleth(area, subarea="Data Zone", boundaries="SG_DataZone_Bdry_2011", metric="Median Distance (km)", name="DataZone", file_type="shp")
# maps.choropleth(area, subarea="Data Zone", boundaries="SG_DataZone_Bdry_2011", metric="Median Distance (km)", name="DataZone", file_type="shp", distance="CC_Distance")
#
##################################################
#
# # Calculate weighted median - i.e. estimation of travel distance for a child in Grampian
# # The Single step analysis csv file was created by taking the aggregated data by datazone for both models and adding the estimated number under 16 years old source: https://www.nrscotland.gov.uk/statistics-and-data/statistics/statistics-by-theme/population/population-estimates/small-area-population-estimates-2011-data-zone-based/time-series - this was done manually in excel
# dz_df = pd.read_csv("Single step analysis.csv")
#
# rach_distances = []  # List of travel distance estimation for all children to RACH
# for index, row in dz_df.iterrows():
#     population = row["Children"]
#     distance = row["Median Distance (km)"]
#     expanded = [distance] * population
#     rach_distances += expanded
# print(np.median(rach_distances))
# print(np.percentile(rach_distances, [25, 75]))
#
# cc_distances = []  # List of travel distance estimation for all children to cluster clinic
# for index, row in dz_df.iterrows():
#     population = row["Children"]
#     distance = row["CC Median Distance (km)"]
#     expanded = [distance] * population
#     cc_distances += expanded
# print(np.median(cc_distances))
# print(np.percentile(cc_distances, [25, 75]))
#
# all_distances = []  # List of travel distance estimation for all children to all centres
# for index, row in dz_df.iterrows():
#     population = row["Children"]
#     distance = row["All Median Distance (km)"]
#     expanded = [distance] * population
#     all_distances += expanded
# print(np.median(all_distances))
# print(np.percentile(all_distances, [25, 75]))
#
# fig = plt.figure()
# box = plt.boxplot([rach_distances, cc_distances, all_distances], vert=False, labels=["RACH",  "Cluster", "All"])
# plt.xlabel("Distance (km)")
# fig.savefig("Boxplot 1")
# fig.show()
#
# print(stats.ks_2samp(np.sort(rach_distances), np.sort(cc_distances)))  # Assess if data is from same distribution
# print(stats.kstest(cc_distances, "norm"))  # determine if travel distance is parametric using kolmogorov-smirnov test
# print(np.median(cc_distances))  # median travel distance for a child in grampian to nearest cluster clinic
# print(np.percentile(cc_distances, [25, 75]))
#
# print(stats.wilcoxon(rach_distances, cc_distances))  # wilcoxon test to assess difference between two medians
#
##############################################################
#
# Calculate weighted Gini coefficient - equality of distribution of cluster clinics for children in Grampian
# distances = all_distances  # change rach_distances or cc_distances as wanted
# distances = np.array(distances, dtype=np.int64)
# distances = np.sort(distances)
# distances += 1
# index = np.arange(1, distances.shape[0]+1)
# number = distances.shape[0]
# gini_coefficient = (float(np.sum((2 * index - number - 1) * distances)) / (number * np.sum(distances)))
# print(gini_coefficient)
#
############################################################
#
# Determine how many are closer and further, and on average how much further people are where RACH is closer
# nearer = 0
# further = 0
# equal = 0
# d_further = 0
# d_further_list = []
# d_nearer_list = []
# for index, row in dz_df.iterrows():
#     population = row["Children"]
#     d1 = row["Median Distance (km)"]
#     d2 = row["CC Median Distance (km)"]
#     if d2 < d1:
#         nearer += population
#         d_nearer_list += [d1-d2] * population
#     elif d1 < d2:
#         further += population
#         d_further += population * (d2 - d1)
#         d_further_list += [d2-d1] * population
#     else:
#         equal += population
# print(nearer, further, equal)  # with cluster clinics - number who will be nearer or further to a service provider
# print(np.percentile(d_further_list, [50, 25, 75]))
# print(np.percentile(d_nearer_list, [50, 25,75]))
# print(d_further/further)  # percentage of children with longer journeys to cluster clinics
# print(np.max(d_list))  # Maximumm increased in travel distance for a child to cluster clinic (median weighted)
#
# con = ps.db_exists(area)
# query = """SELECT Distance, CC_Distance
#            FROM Postcodes
#            WHERE Population IS NOT NULL"""
# distance_df = pd.read_sql_query(query, con)
# furthest = 0
# for index, row in distance_df.iterrows():
#     further = row["CC_Distance"] - row["Distance"]
#     if further > furthest:
#         furthest = further
#         print(furthest)
#     else:
#         pass
# print(furthest)  # Largest increase in travel distance when using cluster clinics (absolute)
#
#########################################################
# # Create bar chart showing population at 5km intervals from centres (children only)
#
# analyse.n_km(area, 5, True, distance="CC_distance", comparison="Distance")  # create graph showing population at 5km intervals from centres (whole population)
#
# max_dist = max(rach_distances)
# results_list = []
# nkm = 5
# measures = ["Distance to RACH", "Distance to Cluster Clinic"]
# for values in (rach_distances, cc_distances):
#     results = {}
#     lower = 0
#     while lower < max_dist:
#         upper = lower + nkm
#         count = 0
#         for d in values:
#             if lower < d <= upper:
#                 count += 1
#             else:
#                 continue
#         key = "%d to %d" % (lower, upper)
#         results[key] = count
#         lower += nkm
#         continue
#     results_list.append(results)
#     width = 0.35
#     m = -1
#     n = 0
# print(results_list)
# for result in results_list:
#     plt.bar(np.array(range(len(list(result.keys())))) + (m * width) / 2, list(result.values()),
#             width=width, label=measures[n])
#     m = -m
#     n += 1
#     plt.xticks(range(len(list(result.keys()))), list(result.keys()), rotation="vertical")
# plt.ylabel("Number")
# plt.legend()
# plt.show()

# analyse.lorenz(area, ["Distance", "CC_Distance"])[0].show()
