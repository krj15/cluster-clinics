## Name
Quantitative evaluation of the impact of the cluster clinic model on geographic service accessibility

## Description
Script writtin in python 3, used in the analysis and evaluation of the cluster clinic model. The code is not structured to be run as a single script, but with appropriate lines silenced to run each process. All data sources are referenced within the code, and would need to be downloaded by the user from the original source.

## Installation
This script heavily relies upon the centresurv package, which must be downloaded to the same repository in which the code is being run.
Other dependencies are listed within the script.

## Usage
Step by step analysis of service accessibility with the cluster clinic model. This is the code that was used specifically in this analysis, and is not designed for onward use, except for those who wish to validate the results. Alternatively, this can be used as a template to guide the user on how to utilise centresurv for comparitave evaluation of health service accessibility.

## Support
For queries, contact kieran.johnstone@nhs.scot

## Authors and acknowledgment
Authored by Kieran Johnstone
Thanks to Professor Steve Turner for his input

## License
Licensed under GPL-3

## Project status
This will not be developed further. It exists solely for reference and transparancy.
